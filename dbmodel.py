from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
host='ec2-54-225-120-137.compute-1.amazonaws.com'
db='d50o3fq3c5v493'
user='ofpptufxpgbgor'
password='jUeHlLmegfrgKVDns9v5UNEc-T'
port = 5432
url = 'postgresql://{}:{}@{}:{}/{}'
url = url.format(user, password, host, port, db)
app.config['SQLALCHEMY_DATABASE_URI'] = url #'sqlite:////tmp/db_python.db'
db = SQLAlchemy(app)
class Companies(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(200), unique=True)
    city = db.Column(db.String(120), unique=False)
    address = db.Column(db.String(300), unique=False)
class Companies_score_list(db.Model):
	id = db.Column(db.Integer, primary_key=True,autoincrement=True)
	name = db.Column(db.String(200), unique=True)
	confidential_score = db.Column(db.Integer)
	city = db.Column(db.String(300))
	headquarter = db.Column(db.String(300))
	data_occ = db.Column(db.Integer)
	company_type = db.Column(db.String(300))
	match_with = db.Column(db.Integer,db.ForeignKey('companies.id'))
class Company_branches(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    company_id = db.Column(db.Integer, db.ForeignKey('companies_score_list.id') ,  unique=False)
    city = db.Column(db.String(120), unique=False)
class Company_designations(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    company_id = db.Column(db.Integer, db.ForeignKey('companies_score_list.id') ,  unique=False)
    designation = db.Column(db.String(120), unique=False)