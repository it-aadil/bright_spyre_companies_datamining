from flask import Flask, render_template, request, session
import json
import requests
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin
from authentication_model import db,User,Role
from flask_security import http_auth_required
from flask import jsonify

# Create app
app = Flask(__name__)
#app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['DEFAULT_MAIL_SENDER'] = 'info@site.com'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_CONFIRMABLE'] = True
app.config['SECURITY_RECOVERABLE'] = True
#configuration for api's
app.config['SECURITY_PASSWORD_HASH']='pbkdf2_sha512'
app.config['SECURITY_TRACKABLE']='True'
app.config['SECURITY_PASSWORD_SALT']='2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824'
app.config['WTF_CSRF_ENABLED']='False'

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


# Create a user to test with
@app.before_first_request
def create_user():
    db.create_all()
    if not User.query.first():
        user_datastore.create_user(email='test@example.com', password='test123')
        db.session.commit()

@app.route('/')
def index():
	return 'Home Page'

@app.route('/dummy-api/', methods=['GET','POST'])
@http_auth_required
def dummyAPI():
    r = {'status':'success'}
    return json.dumps(r)

if __name__ == '__main__':
    app.run('0.0.0.0')
