from flask import Flask, url_for, render_template,request
app = Flask(__name__)
import sys
import sqlalchemy
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from dbmodel import db,Companies,Companies_score_list,Company_branches,Company_designations
import psycopg2
import json
from fuzzywuzzy import fuzz
from flask_redis import Redis

# For background Process Start
from rq import Queue
from worker import conn
q = Queue(connection=conn)
from myapp import addToBackground
#For background Process End

reload(sys)  
sys.setdefaultencoding('UTF8')

@app.route('/')
def index():
	return 'Companies Page'

@app.route('/testing')
def testing_testing():
	addToBackground()
	return 'Companies Page'



@app.route('/tokenbase-authentication',methods=['GET','POST'])
def tokenbase():
	return "Nothing in route"
	
@app.route('/add-companies-to-db-from-text-file')
def models_testing_phase():
	count = 0
	#db.create_all()
	with open('data/companiesdata.txt','r') as f:
	 	content = f.read()
	 	lines = content.split("\n")
    	for line in lines:
    		count = count + 1
    		arr = line.split("===")
    		if len(arr)==3:
	    		name = arr[0].strip()
	    		city = arr[1].strip()
	    		address = arr[2].strip()
	    		name = name.lower()
	    		city = city.lower()
	    		address = address.lower()
	    		company = Companies()
	    		check_existence = Companies.query.filter_by(name=name).all()
	    		if not check_existence:
	    			company.name=name
		    		company.city=city
		    		company.address=address
		    		db.session.add(company)
		    		db.session.commit()
		    	print "Lines Processed:"+str(count)
	return 'model created'
def connect_db():
	conn_string = "host='ec2-54-225-120-137.compute-1.amazonaws.com' dbname='d50o3fq3c5v493' user='ofpptufxpgbgor' password='jUeHlLmegfrgKVDns9v5UNEc-T'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	return conn,cursor
def connect_db_local():
	conn_string = "host='localhost' dbname='db_python' user='aadil' password='cogilent123'"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	return conn,cursor
def connect(user, password, db, host='ec2-54-225-120-137.compute-1.amazonaws.com', port=5432):
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)
    con = sqlalchemy.create_engine(url, client_encoding='utf8')
    meta = sqlalchemy.MetaData(bind=con, reflect=True)
    return con, meta
def calculate_confidential_score(company_name,master_list):
	max_matching = -1
	address = ''
	city_city = ''
	match_id = -1
	tst = ''
	#print "Lenght of master List :"+str(len(master_list))
	for ob in master_list:
		tmp_score = fuzz.token_sort_ratio(company_name,ob.name)
		#print str(tmp_score) + company_name +"=="+ ob.name
		if tmp_score > max_matching:
			max_matching = tmp_score
			address = ob.address
			city_city = ob.city
			match_id = ob.id
			tst = ob.name
	#print company_name + "==matched with=="+ tst + "=="+str(max_matching)
	return max_matching,address,city_city,match_id
	#confidential_score,address,city_2,matching_id = 
@app.route('/make-confidential-score')
def make_confidential_score():
	db.create_all()
	con,cur = connect_db()
	increment = 500
	q = "select location from tb_pointer where name = 'position5'"
	cur.execute(q)
	data = cur.fetchall()
	start = data[0][0]
	print "Data read from =>"+str(start)+" and limit =>" +str(increment)
	que = "select company_name,city,country,designation,company_type from experience limit %d offset %d" % (increment,start)
	cur.execute(que)
	data = cur.fetchall()
	if not data:
		return "0"
	q = "update tb_pointer set location='%d' where name='position5'" % (start+increment)
	cur.execute(q)
	counter = 0
	for row in data:
		counter = counter + 1
		company_name = ''
		city = 'none'
		country = 'none'
		designation = 'none'
		company_type = 'none'
		if row[0]:
			company_name = row[0].strip().lower()
		if row[1]:
			city = row[1].strip().lower()
		if row[2]:
			country = row[2].strip().lower()
		if row[3]:
			designation = row[3].strip().lower()
		if row[4]:
			company_type = row[4].strip().lower()
		if company_name:
			check_existence = Companies_score_list.query.filter_by(name=company_name).first()
			id_of_new_row = -1
			if check_existence:
				#print "alrady exists"
				#increment existence of that row
				#val = check_existence.data_occ
				#check_existence.data_occ = val + 1
				#check_existence.save()
				id_of_new_row = check_existence.id
				#Companies_score_list.update().where(Companies_score_list.id=check_existence.id).value()
				Companies_score_list.query.filter_by(name=company_name).update(dict(data_occ=check_existence.data_occ+1))

			else:
				char = company_name[0:1]
				#print "Comapny name :==" +company_name[0:1] 
				if char.isdigit() or char.isalpha():
					master_list = Companies.query.filter(Companies.name.startswith(company_name[0:1])).all()
					#print len(master_list)
					confidential_score,address,city_2,matching_id = calculate_confidential_score(company_name,master_list)
					#create object to be added
					obj_add = Companies_score_list()
					
					obj_add.name = company_name
					obj_add.confidential_score = confidential_score
					obj_add.headquarter = address
					obj_add.data_occ = 1
					obj_add.company_type = company_type
					if len(obj_add.company_type) >299:
						obj_add.company_type = obj_add.company_type[0:299]
					obj_add.match_with = matching_id
					obj_add.city = city_2
					db.session.add(obj_add)
					id_of_new_row = Companies_score_list.query.filter_by(name=company_name).first().id
			#create new branch of that company
			branch_check = Company_branches.query.filter_by(city=city,company_id=id_of_new_row).first()
			#print "Id of new Row :"+str(id_of_new_row)
			if not branch_check:
				#print "Adding branch"
				branch = Company_branches()
				branch.company_id = id_of_new_row
				branch.city= city
				db.session.add(branch)
			#create new designation of that company
			designation_check = Company_designations.query.filter_by(designation=designation,company_id=id_of_new_row).first()
			if not designation_check:
				desg = Company_designations()
				desg.company_id = id_of_new_row
				desg.designation = designation
				db.session.add(desg)
		print "Row processed \t:\t"+str(counter) +" Rows remaining to process : "+str(increment-counter)
	#print que
	db.session.commit()
	con.commit()
	return "1"

@app.route('/reset-recylce-pointer')
def reset_recycle_pointer():
	con , cur = connect_db()
	query = "update tb_pointer set location = 0 where name='position5_reprocess'"
	cur.execute(query)
	con.commit()
	return "Pointer Reset to 0, successfull !!"

@app.route('/rematch-companies')
def loop_recycling_companies():
	while True:
		status = rematch_companies()
		if status == '0':
			print "status 0 "
			return "All data Recycled please , if you have added more companies then please visit this link ^/reset-recylce-pointer^ once and then again visit /rematch-companies"
		elif status == '1':
			print "status 1 "
			loop_recycling_companies()
		else:
			print "status Error "
			return "Error Occured"

@app.route('/rematch-companies-route')
def rematch_companies():
	query_pointer = "select location from tb_pointer where name = 'position5_reprocess'"
	con,cur = connect_db()
	cur.execute(query_pointer)
	pointer = cur.fetchone()
	limit = 100
	query_fetch_companies = "select id,name,confidential_score from companies_score_list where confidential_score < 80 limit %d offset %d" % (limit ,int(pointer[0]))
	query_update_pointer = "update tb_pointer set location='%d' where name='position5_reprocess'" % (int(pointer[0])+limit)
	cur.execute(query_update_pointer)
	print query_fetch_companies
	cur.execute(query_fetch_companies)
	companies = cur.fetchall()
	if not companies:
		return '0'
	for company in companies:
		company_id = company[0]
		company_name = company[1]
		company_confidence = company[2]
		char = company_name[0:1]
		if char.isdigit() or char.isalpha():
			master_list = Companies.query.filter(Companies.name.startswith(company_name[0:1])).all()
			#print len(master_list)
			confidential_score,address,city_2,matching_id = calculate_confidential_score(company_name,master_list)
			if confidential_score > company_confidence:
				print "changed occured => " + company_name + " ::"+ str(company_confidence) + "::" + str(confidential_score)
				query_update = "update companies_score_list set match_with=%d, confidential_score = %d where id=%d" % (matching_id,confidential_score,company_id)
				cur.execute(query_update)
			else :
				print "No change occured => " + company_name + " ::"+ str(company_confidence) + "::" + str(confidential_score)
	con.commit()
	return "1"

		

	


@app.route('/start-loop')
def loop_function():
	while True:
		status = make_confidential_score()
		if status == "0":
			return "All data Processed"
		else:
			print  "\t\tOne Loop Ended "
@app.route('/check-api')
def api_testing():
	return render_template("show_data.html")
@app.route('/ajax-call',methods=['GET','POST'])
def ajax_call():
	db.create_all()
	#con,cur = connect_db()
	value = request.args['val']
	status = request.args['st']	
	#status value description below:
	#r1 for branches of company
	#r2 for jobs position in company
	#r3 for companies by company type
	out = []
	if status=="r3":
		#print "yaho o"
		count =0
		result = Companies_score_list.query.filter(Companies_score_list.company_type.startswith(value)).all()[:50]
		for obj in result:
			count = count + 1
			#print obj.name
			name = obj.name
			out.append(name)
	elif status=="r1":
		count = 0 
		result = Companies_score_list.query.filter_by(name=value).first()
		if result:
			result2 = Company_branches.query.filter_by(company_id=result.id).all()
			for obj in result2:
				count = count + 1
				name = obj.city.replace(",","")
				out.append(name)
	elif status=="r2":
		count = 0 
		result = Companies_score_list.query.filter_by(name=value).first()
		if result:
			result2 = Company_designations.query.filter_by(company_id=result.id).all()
			for obj in result2:
				count = count + 1
				name = obj.designation.replace(",","")
				out.append(name)
	elif status=="r4" :
		con,cur=connect_db()
		qq = """
			select 
				min(tb_degrees_list.name),max(mast_occ),sum(data_occ),match_with
					from 
				degrees inner join tb_degrees_list on match_with = tb_degrees_list.id
					where 
				mast_occ > 40 group by match_with
			"""
		#qq = "select "
			#degrees.name like 'bach%' and
		cur.execute(qq)
		ooo = []
		dd = cur.fetchall()
		for val in dd :
			ooo.append(val[0])
		return json.dumps(ooo)
			



		#return "ddd"
		q = "select match_with from tb_degrees_score_list where name='%s' and mast_occ > 40" %(value)
		cur.execute(q)
		#out = []
		data = cur.fetchall()
		#return "ddd"
		if data:
			#print "step 1"
			m_id = data[0][0]
			q = "select id,name from tb_degrees_score_list where mast_occ > 40 and match_with = '%d'" %(m_id)
			cur.execute(q)
			data = cur.fetchall()

			if data:
				#print "step 2"
				for mm in data:
					q = "select distinct(institute_id) from tb_specialization where degree_id = '%d' " %(mm[0])
					cur.execute(q)
					dd_v = cur.fetchall()
					#print len(dd_v)
					for dd in dd_v:
						qq = "select name from institutes where id='%d'" %(dd[0])

						cur.execute(qq)
						tmp = cur.fetchall()
						if tmp:
							#print "step 3"
							out.append(tmp[0][0])
			else :
				out.append("no more combinations")
		else :
			out.append("not even matched")
		print "Ending call"
				

	return json.dumps(out)
@app.route('/auto-fill',methods=['GET','POST'])
def auto_fill():
	db.create_all()
	con,cur = connect_db_local()
	value = request.args['val'].lower()
	out = []
	value = value + "%"
	q = """ 
	select
		distinct(Companies.name) as Fname,
		sum(data_occ) as data_occ,
		max(confidential_score) as confidential_score
			from
		Companies_score_list
		   inner join
		       Companies on match_with=Companies.id
		         where 
		         	match_with is not null and 
		         	not match_with = -1 and 
		         	Companies_score_list.name like '%s' and 
		         	confidential_score > 85  
		         		group by Fname 
		         		order by confidential_score 
		         		limit 30
		;
	"""%(value) 
	print q
	cur.execute(q)
	data = cur.fetchall()
	for val in data:
		tmp = val[0]
		out.append(tmp)
		#print tmp

	result = Companies_score_list.query.filter(Companies_score_list.name.startswith(value),Companies_score_list.confidential_score>90).all()[:30]
	for obj in result:
		#print obj.name
		name = obj.name
		#out.append(name)
	return json.dumps(out)
if __name__ == '__main__':
    app.run(host='0.0.0.0')
